In the event of power failure, the microHAT provides enough time to back up and save files. 
In this scenario the microHAT is connected to the PI, which is being used as a backup server, with the microHAT adding the UPS functionality.
 In the event that the user is not present and the PI is running the microHAT will implement an automatic save of data before shutting down properly without losing unsaved information.
  The use case requires approximately 30 minutes of power supply in order to effectively shut down the PI server.

**BILL OF MATERIALS**
- microHAT case
- USB-c Cable
- Wires
- Header
- Raspberry Pi 
